const _ = require('lodash');
const facts = require('./facts.json')

//facade pattern
class Comment {
    constructor(type) {
      this.type = type;
    }

    create(playersArr, user) {
      const playerAmount = playersArr.length;   
      const orderedArr = orderArr(playersArr);

      switch(this.type) { 
        case 'start': {
          let comment = `Мене звати Булка, і я радий вас вітати зі словами 
          доброго Вам дня панове! Оголошую список учасників цієї гонки: `; 
          _.forEach(playersArr, (player, i) => {
            if (i == 0) comment += `перший учасник - це ${player.user} на ${player.car} `
            else if (i == playerAmount - 1) comment += `і, нарешті, приєднався ${player.user} на ${player.car}.`;
            else comment += `, ${player.user} на своєму ${player.car}, `;
          });
          return comment;
        };

        case 'each 30 sec': {
          let comment = 'На даний момент учасники займають такі позиції: ';
          _.forEach(orderedArr, (player, i) => {
            if (player.time != 0) comment += `${player.user} вже чекає на фініші `;
            else if (i == 0) comment += `першим їде ${player.user} `;
            else if (i == playerAmount - 1) comment += `і останнім їде ${player.user}. `;
            else comment += `, за ним ${player.user}, `;
          });
          if (playerAmount != 1) {
            let distance = getDistance(orderedArr);
            if (distance != 0)
            comment += `Відстань між першим і останнім учасниками ${distance} м.`;
          }
          return comment;
        }; 

        case 'last 30 symbols': {
          let comment = '';
          const playersNames = _.map(orderedArr, _.iteratee('user'));
          if (user == playersNames[0])
          comment = `${user} схоже збирається фінішувати першим.`;
          else comment = `${user} наближається до фінішу`;
          return comment;
        };

        case 'final comment': {
          let comment = '';
          const playersNames = _.map(orderedArr, _.iteratee('user'));
          if (user == playersNames[0])
          comment = `Схоже у нас є переможець! Фінішну пряму першим пересікає ${user}`;
          else if (user == playersNames[playerAmount - 1]) 
          comment = `І останнім фінішує ${user}!`;
          else comment = `${user} пересікає фінішну пряму!`;
          return comment;
        };

        case 'results': { 
          let comment = ''; 
          let n;
          if (playerAmount < 3) n = playerAmount;
          else n = 3;
          for (let i = 0; i < n; i++) {
            let player = playersArr[i];
            if (player.time != 0) 
            comment += `${player.user} зайняв ${i+1} місце, пройшовши всю дистанцію 
            за ${miliToSeconds(player.time)} сек. `;
            else comment += `${player.user} зайняв ${i+1} місце, пройшовши ${player.progress} м. `
          }
          comment += 'З вами був Булка, до нових зустрічей!';
          return comment;
        };

        case 'accident': {
            let comment = `Не може бути! ${user} потрапив у аварію! Дуже шкода...`;
            return comment;
        };

        case 'random fact': {
            let factId = getRandomInt(10);
            const comment = _.find(facts, fact => fact.id === factId).text;
            return comment;
        }
      }
    }
  };

  const miliToSeconds = (time) => {
      return Math.round(time/1000);
  };

  const getDistance = (playersArr) => {
      const last = playersArr.length - 1;
      if (playersArr[0])
      return playersArr[0].progress - playersArr[last].progress;
  };

  const orderArr = (playersArr) => {
      return  _.orderBy(playersArr, ['progress', 'time'], ['desc', 'asc']);
  };

  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  module.exports = Comment;

  