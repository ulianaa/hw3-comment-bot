const Comment = require("./bot.facade");
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const users = require('./users.json');
const path = require('path');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const maps = require('./maps.json');
const cars = require('./cars.json')
const _ = require('lodash');


require('./passport.config');

server.listen(3000);

let mapId;

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/game',  (req, res) => {
    res.sendFile(path.join(__dirname, 'game.html'));
});

app.post('/login', function (req, res) {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.login === userFromReq.login);
    if (userInDB && userInDB.password === userFromReq.password) {
      const token = jwt.sign(userFromReq, 'someSecret');
      res.status(200).json({ auth: true, token });
    } else {
      res.status(401).json({ auth: false });
    }
});

app.post('/game', passport.authenticate('jwt', {session: false}), function (req, res) {
  const currMap = _.find(maps, map => map.id === mapId);
  res.json({ currMap, textId: mapId });
});


let players = 0, timer, gameStatus = 'off', playersArr = [];
let endOfGame;

io.on('connection', socket => {

  //handler for proxy
  const validator = {
    set: function (obj, prop, value) {
      if (prop == 'progress' || prop == 'time') {
        if (!Number.isInteger(value)) {
          throw new Error(`${prop} is not an integer`);
        }
        else obj[prop] = value;
      }
      else if (prop == 'user') {
        const user = jwt.verify(value, 'someSecret');
        if (user) obj[prop] = user.login;
        else throw new Error('Token is not valid');
      }
      else obj[prop] = value;
      return true;
    }
  };

  //factory pattern
  class UserFactory {
    create(userLogin) {
      //proxy
      let player = new Proxy({}, validator);
      player.progress = 0;
      player.time = 0;
      player.user = userLogin; 
      player.id = socket.id;
      const carId = getRandomInt(6);
      const userCar = _.find(cars, car => car.id === carId);
      player.car = userCar.car;
      return player;
    }
  };
  const userFactory = new UserFactory();

  players++;
  console.log(`players: ${players}`);
  console.log(gameStatus);
   
  const startGame = () => {
    playersArr = [];
    mapId = getRandomInt(3);
    io.to('wait').emit('quit room');
    io.to('play').emit('start timer');
    io.to('play').emit('start game');
    let now = _.now();
    endOfGame = now + 185000;
  };
  if (gameStatus == 'on') {
    socket.join('wait');
    let now = _.now();
    socket.emit('timer for wait-room', { time: endOfGame - now });
    console.log('user joined wait room');
  } else {
    socket.join('play');
    if (players == 1) {
      setTimeout(startGame, 7000);
      timer = setInterval(startGame, 195000);
    }
  }


  socket.on('change game status', payload => {
    gameStatus = payload.status;
    console.log(gameStatus);
  }); 

  socket.on('new user', payload => {
    const { token } = payload;
    let newPlayer = userFactory.create(token);
    playersArr.push(newPlayer);
    io.to('play').emit('new player', { user: newPlayer.user });
  });

  socket.on('join game', payload => {
    const { token } = payload;
    const user = jwt.verify(token, 'someSecret');
    if (user) {
      const userLogin = user.login;
      socket.leave('wait');
      socket.join('play');
      console.log(`${userLogin} left wait-room`);
    }   
  });

  socket.on('confirm correct symbol', payload => {
    const { token } = payload;
    const user = jwt.verify(token, 'someSecret');
    
    if (user) {
      const userLogin = user.login;
      for (let key in playersArr) {
        if (playersArr[key].user == userLogin) 
        playersArr[key].progress++;
      }
      io.emit('correct symbol', { user: userLogin });
    }
  });

  socket.on('user finished the text', payload => {
    const { token } = payload;
    const user = jwt.verify(token, 'someSecret');
    if (user) {
      const userLogin = user.login;
      let now = _.now();
      let time = 180000 - endOfGame + now;
      for (let key in playersArr) {
        if (playersArr[key].user == userLogin) 
        playersArr[key].time = time;
      }
      let comment = new Comment('final comment');
      io.to('play').emit('show comment', { comment: comment.create(playersArr, userLogin) });
    }
  });

  socket.on('the end', () => {
    const orderedArr = _.orderBy(playersArr, ['progress', 'time'], ['desc', 'asc']);    
    console.log(orderedArr);
    socket.emit('results', { players: orderedArr });
  });

  socket.on('disconnect', () => {
    players--;
    for (let key in playersArr) {
      if (playersArr[key].id == socket.id) {
        let player = playersArr[key].user;
        io.to('play').emit('player disconnected', { user: player });
        const comment = new Comment('accident');
        io.to('play').emit('show comment', { comment: comment.create(playersArr, player) });
        playersArr.splice(key, 1);
      }
    }
    delete playersArr[socket.id];
    if (players == 0) {
      clearInterval(timer);
      gameStatus = 'off';
    }
    console.log(`players: ${players}`);
  });

  socket.on('get start comment', () => {
    const comment = new Comment('start');
    io.to('play').emit('show comment', { comment: comment.create(playersArr) });
  });

  socket.on('get 30 symb comment', payload => {
    const { token } = payload;
    const user = jwt.verify(token, 'someSecret');
    if (user) {
      const userLogin = user.login;
      const comment = new Comment('last 30 symbols');
      io.to('play').emit('show comment', { comment: comment.create(playersArr, userLogin) });
    }
  });

  socket.on('get 30 sec comment', () => {
    const comment = new Comment('each 30 sec');
    io.to('play').emit('show comment', { comment: comment.create(playersArr) });
  });

  socket.on('get results', () => {
    const comment = new Comment('results');
    io.to('play').emit('show comment', { comment: comment.create(playersArr) });
  });

  socket.on('get random fact', () => {
    const comment = new Comment('random fact');
    io.to('play').emit('show comment', { comment: comment.create(playersArr) });
  });

});

function getRandomInt(max) {

  return Math.floor(Math.random() * Math.floor(max));

};






